package no.ntnu.imt3281.sudoku.exception;

/**
 * Exception klasse for når bruker prøver å taste inn ugyldige verdier
 */
public class BadNumberException extends RuntimeException{

    /**
     * Constructor caller RuntimeException sin constructor
     * @param msg feilmeldingen
     */
    public BadNumberException(String msg) {
        super(msg);
    }

    /**
     *
     * @param msg Feilmelding
     * @param r Rad nummer
     * @param k Kolonne nummer
     * @param b Box index
     * @param index Index som insettes i
     */
    public BadNumberException(String msg, int r, int k, int b, int index) {
        super(msg + (r == -1 ? "" : "Rad " + (r-index) + "\n") +
        (k == -1 ? "" : "Kolone " + (k-index)/9 + "\n") +
                (b == -1 ? "" : "Box " + b + "\n"));
    }
}
