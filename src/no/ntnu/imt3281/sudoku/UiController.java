package no.ntnu.imt3281.sudoku;

import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import no.ntnu.imt3281.sudoku.exception.BadNumberException;
import no.ntnu.imt3281.sudoku.logic.SudokuLogic;
import no.ntnu.imt3281.sudoku.logic.cell.Rute;
import no.ntnu.imt3281.sudoku.logic.operation.BoardSearcher;
import no.ntnu.imt3281.sudoku.logic.operation.BoardValidator;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


/**
 * <h1>UiController</h1>
 * <p>Denne klassen tar av seg ui elementer
 * i dette sudoku bretter</p>
 */
public class UiController {

    private SudokuLogic logic;

    //box either starts 0, 3, 6
    private static final int BOXSTART = 3;


    @FXML
    private  GridPane grid;
    /**
     * Denne funksjonen tar av seg av alle fxml objekter
     * og setter opp sudoku brettet vårt
     */
    @FXML
    private void initialize() {
        grid.setHgap(10);
        grid.setVgap(8);
        grid.setGridLinesVisible(Boolean.TRUE);
        logic = new SudokuLogic();

        fillGrid();
    }

    /**
     * Denne  funksjonen går gjennom griden og fyller gridpanes
     * i en 3x3 gridpane
     */
    private void fillGrid() {
        int boxRowCount = 0;

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                switch (i){
                    // Dette er for å sjekke hvilken box av de 3
                    // ved siden av hverandre
                    case 0: boxRowCount = i+j; break;
                    case 1: boxRowCount = BOXSTART+j; break;
                    case 2: boxRowCount = 2*BOXSTART+j; break;
                    default: break;
                }
                // henter et iterator objekt
                Iterator boxIter = logic.getBoxValuesIterator(BoardSearcher.getBIndex(boxRowCount));
                GridPane box = new GridPane();
                box.setHgap(4);
                box.setVgap(4);
                box.setGridLinesVisible(Boolean.TRUE);
                box.setStyle("-fx-border-color: black");


                addTextfield(boxIter, box);
                GridPane.setConstraints(box, j, i);
                grid.getChildren().add(box);
            }
        }
    }

    /**
     * Denne funksjonen action listner som sitter på hvert
     * textfield objekt og sjekker om input er riktig og
     * om hele brettet er fult hvis det er ingen feil
     */
    private ChangeListener<String> textListener = (ObservableValue<? extends String> observableValue, String oldValue, String newValue) ->{
        StringProperty prop = (StringProperty)observableValue;
        TextField field = (TextField) prop.getBean();
        boolean ok = checkNewValue(newValue);
        if(Boolean.TRUE.equals(ok)) {
            Integer value = (newValue.equalsIgnoreCase("")) ? -1 : Integer.valueOf(newValue);

            Integer index;
            if (value >= -1 && value <= 9) {
                index = Integer.valueOf(field.getId());
                List<Integer> feil = logic.input(index, value);
                if (feil.isEmpty()) {
                    field.setStyle("-fx-background-color: white");
                    BoardValidator validator = new BoardValidator();
                    if(validator.won(logic.getSudoku().iterator())) {

                        Locale currentLocale;
                        ResourceBundle messages;

                        // finner messages og henter ut fra språk og land hva
                        // slags messages som blir skrevet
                        String s = Locale.getDefault().toString();
                        String[] str = s.split("_");
                        currentLocale = new Locale(str[0], str[1]);

                        messages = ResourceBundle.getBundle("no.ntnu.imt3281.sudoku.i18n.Won", currentLocale);

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);

                        alert.setTitle(messages.getString("title"));
                        alert.setContentText(messages.getString("context"));
                        logic.startNewGame();
                        grid.getChildren().clear();
                        fillGrid();
                        alert.showAndWait();
                    }
                } else {
                    field.setStyle("-fx-background-color: rgba(255, 0, 0, 0.5)");
                    throw new BadNumberException("Feilene : ", feil.get(0), feil.get(1), feil.get(2), index);
                }
            } else {
                field.setText("");

            }
        } else if(field.getText().isEmpty()) {
            field.setStyle("-fx-background-color: white");


        } else {
            field.setStyle("-fx-background-color: rgba(255, 0, 0, 0.5)");
            field.setText("");
           throw new BadNumberException("ikke lov med bokstaver eller symboler");
        }
    };

    /**
     * Denne  funksjonens jobb er å legge til 9 textfield objekter inn i
     * en gridpane og legge den til inn i gridpanen fra FXML filen
     * @param boxIter Denne tar med iteratoren så vi kan bruke den
     *                for å hente ruter
     * @param box Dette er en 3x3 gridpane som vi vill fylle 9
     *            textfields i.
     */
    private void addTextfield(Iterator boxIter, GridPane box) {

        String field;
        for(int kolone = 0; kolone < 3; kolone++) {
            for(int rad = 0; rad < 3; rad++) {
                Rute rute = (Rute)boxIter.next();
                field = (rute.toString().equalsIgnoreCase("-1")) ? "" : rute.toString();

                TextField textField = new TextField(field);
                if (rute.isLocked()) {
                    textField.setStyle("-fx-background-color: rgba(200,200,200,0.5)");
                }
                textField.setId(rute.getIndex().toString());
                textField.setEditable(!rute.isLocked());
                textField.textProperty().addListener(textListener);
                textField.setPrefHeight(100);
                GridPane.setConstraints(textField, rad, kolone);
                box.getChildren().add(textField);

            }
        }
    }

    /**
     *
     * @param newValue Denne verdien er den ny endret verdien når en bruker
     *                 endrer verdien i en textfield.
     * @return returner
     */
    private Boolean checkNewValue(String newValue) {
        boolean ok = true;
        try {
            Integer.parseInt(newValue);
        } catch (NumberFormatException|NullPointerException e) {
            ok = false;
        }
        if(newValue.equalsIgnoreCase("")){
            ok = true;
        }
            return ok;
    }


}
