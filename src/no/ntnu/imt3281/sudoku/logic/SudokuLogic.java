package no.ntnu.imt3281.sudoku.logic;

import no.ntnu.imt3281.sudoku.logic.cell.Rute;
import no.ntnu.imt3281.sudoku.logic.operation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <h1>logikk klassesn</h1>
 * <p>Logikken innholder brettet og vil ta for seg de forskjellige
 * iteratorene for å finne rad og slikt.
 * vil bruke operation klassene som implementerer Iterator</p>
 *
 * */
public class SudokuLogic {

    private static final Logger LOGGER =  Logger.getLogger(SudokuLogic.class.getName());

    // liste med alle cellene
    private List<Rute> sudoku;
    /**
     * Denne konstruktoren tar seg av å initalisere alle operation
     * objektene og arraylista.
     *
     */
    public SudokuLogic() {
        // initialiserer arraylista
        sudoku = BoardTransformer.startNewGame();

        // setting the logger level
        LOGGER.setLevel(Level.FINEST);
    }

    /**
     * Alternativ constructor for testing sin skyld.
     * Genrerer lista som den er i json filen, istede for en tilfeldig speiling
     * @param b Mningsløs parameter, er det bare for overloading sin skyld
     */
    public SudokuLogic(boolean b){
        // initialiserer arraylista
        sudoku = BoardTransformer.testLoad();
        // setting the logger level
        LOGGER.setLevel(Level.FINEST);
    }

    public Iterator getBoardIterator(int start, String type){
        return new BoardIterator(start, sudoku, type);
    }

    public Iterator getBoxValuesIterator(int start){
        return new BoxValuesIterator(start, sudoku);
    }

    /**
     * Viser brettet
     */
    public static void show(List<Rute> l){
        for(int i = 0; i < l.size(); i+=9){
            List<Rute> sl = l.subList(i, i+9);
            LOGGER.finest(()-> sl + "\n");
        }
    }

    /**
     * Returnerer soduku brettet
     * @return Listen med elementene
     */
    public List<Rute> getSudoku(){
        return sudoku;
    }

    /**
     * Setter inn en verdi i listen, og dømmer om den er gyldig
     * @param ind index av rute som skal ha en verdi insatt
     * @param value verdien som skal bli insatt
     */
    public List<Integer> input(int ind, int value){
        List<Integer> rkb = BoardSearcher.getRkb(ind);                  //Array med radnummer, kollonne nummer og index til boks
        Rute rute;                                                      //Ruten vi jobber på
        ArrayList<Integer> feil = new ArrayList<>();                    //Feilmelding
        int r = validate(value, rkb.get(0)*9, "row");       //Validerer rad, kollonne, og boks
        int k = validate(value, rkb.get(1), "column");
        int b = validate(value, rkb.get(2), "box");
        if(r == -1 && k == -1 && b == -1){                              //ser hvis rad, kolonne og boks er valid
            rute = new Rute(value, true, ind);                       //Oppdaterer rute
            sudoku.set(ind, rute);
        }
        else{                                                           //Fyller ut feil
            feil.add(r);
            feil.add(k);
            feil.add(b);

        }
        return feil;
    }

    private Integer validate(int value, int start, String type){
        BoardValidator validator = new BoardValidator();                //Lager en validator
        Iterator iter = getBoardIterator(start, type);                  //Lager iterator for raden/boksen/kolonnen

        return validator.valid(value, iter);                            //Validerer raden/kolonnen/boksen
    }

    /**
     * Start new game lager et nytt brett så vi kan resete bordet
     */
    public void startNewGame(){
        sudoku = BoardTransformer.startNewGame();
    }

}
