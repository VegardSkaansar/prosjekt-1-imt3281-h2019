package no.ntnu.imt3281.sudoku.logic.operation;

import no.ntnu.imt3281.sudoku.logic.cell.Rute;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Klassens oppgave er å iterere rader, kolloner eller bokser i brettet
 */
public class BoardIterator implements Iterator {

    private ArrayList<Rute> values;     //Verdiene i raden/kolonnen/boksen
    private int pos;                    //Posisjonen i listen

    /**
     * Constructor som initisaliserer "values".
     * Avhengig av type, leses enten verdiene i raden, kolonnen eller boxen
     * Teller ikke med blanke verdier
     * @param start Index til første element i rad/kolonne/box
     * @param list Listen med verdiene i brettet
     * @param type Hva slags liste som skal bygges (rad, kollone, boks)
     */
    public BoardIterator(int start, List<Rute> list, String type){
        values = new ArrayList<>();                                 //Verdier som ikke er blanke
        pos = 0;                                                    //Variabel for å se om iterator er ferdig
        switch (type.toLowerCase()){
            case "row":
                row(list, start);
                break;

            case "column":
                column(list, start);
             break;
            case "box":
                box(list, start);
              break;
            default:
                break;
        }
    }

    /**
     * Implementerer hasNext() fra Iterator.
     * @return true/false om det finnes flere elementer
     */
    @Override
    public boolean hasNext() {
        return (pos < values.size());
    }

    /**
     * Implementerer next() fra Iterator
     * @return Det neste objektet i values
     */
    @Override
    public Object next() {
        if(!hasNext()){ throw new NoSuchElementException();}
        Rute c = values.get(pos);
        pos++;
        return c;
    }

    private void row(List<Rute> list, int start) {
        for(int i = start; i < start+9; i ++){              //Fyller values med alle ikke blanke verdier i raden
            if(list.get(i).getValue() != -1){
                values.add(list.get(i));
            }
        }

    }

    private void column(List<Rute> list, int start) {
        for (int i = start; i <= start + 72; i += 9) {           //Fyller values med alle ikke blanke verdier i kolonnen
            if (list.get(i).getValue() != -1) {
                values.add(list.get(i));
            }
        }
    }

    private void box(List<Rute> list, int start) {
        for(int i = start; i < (start+20); i+=9){           ////Fyller values med alle ikke blanke verdier i boksen
            for(int j = i; j < (i+3); j++){
                if(list.get(j).getValue() != -1){
                    values.add(list.get(j));
                }
            }
        }
    }


}

