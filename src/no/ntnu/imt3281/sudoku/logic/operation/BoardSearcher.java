package no.ntnu.imt3281.sudoku.logic.operation;

import java.util.ArrayList;
import java.util.List;

/**
 * Klassens oppgave er å søke gjennom brettet og returnere posisjoner, ved å bruke en gitt en index
 * Inneholder statiske metoder for å gjøre denne jobben
 */
public class BoardSearcher {

    private BoardSearcher() {
        // Denne er for å gjemme konstruktoren
    }
    /**
     * Tar inn en index og beregner hvilken rad, kolonne og box i brettet den indexen befinner seg i
     * @param ind dette er indexen i listen
     * @return en array med indexene til første element i raden, kolonnen og boxen.
     */
    public static List<Integer> getRkb(int ind){
        ArrayList<Integer> res = new ArrayList<>();
        int r = ind/9;                                                  //Beregner nummer
        int k = ind;                                                    //Kolonnen er i utgangspunket index
        int b = 0;                                                      //Boks indexen
        ArrayList<Integer> bIndx = getBIndx();                          //Array med indexer til starten på hver boks
        for(int i = 0; i < bIndx.size(); i++){                          //Søker gjennom hver boks til en match fins
            for(int j = bIndx.get(i); j < (bIndx.get(i)+27); j+=9){
                for(int l = j; l < j+3; l++){
                    if(ind == l){
                        b = bIndx.get(i);                               //Setter b
                    }
                }
            }
        }
        while((k-9) >= 0){                                              //Jobber seg oppover fra index, til kolonnen er funnet
            k-=9;
        }
        res.add(r);
        res.add(k);
        res.add(b);
        return res;
    }

    /**
     * Tar inn et nummer, og returnerer hva som er start index for den gitte boksen
     * @param num nummeret av hvilken boks det gjelder (boks 0, 1, ...)
     * @return start index for boksen
     */
    public static int getBIndex(int num){
        ArrayList<Integer> bIndexer = getBIndx();
        return bIndexer.get(num);
    }

    /**
     * Beregner indexene til første elemente i hver box
     * @return indexene til de første elementene i hver box
     */
    private static ArrayList<Integer> getBIndx(){
        ArrayList<Integer> res = new ArrayList<>();
        for(int i = 0; i < (27*3); i+=27){              //Jobber seg langs ver rad av bokser
            for(int j = i; j < i+9; j+=3){              //Jobber seg langs hver kolonne av bokser
                res.add(j);                             //Henter index til første element i boksen
            }
        }
        return res;
    }
}
