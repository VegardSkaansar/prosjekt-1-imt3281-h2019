package no.ntnu.imt3281.sudoku.logic.operation;

import no.ntnu.imt3281.sudoku.logic.cell.Rute;

import java.util.Iterator;

/**
 * Klassens oppgave er å sjekke input til sudoku brette. Den sjekker om insettingen
 * er gyldig.
 */
public class BoardValidator {

    /**
     * Tar inn en iterator og en insettings verdi, så ser om insettings verdien er unik
     * i raden
     * @param v Verdien som skal bli satt inn
     * @param it Iterator for enten rad, colone eller box
     * @return true/false om verdien i raden er gyldig
     */
    public Integer valid(int v, Iterator it){
        Integer ok = -1;                                               //int som returneres
        while (it.hasNext()){                                            //Kjører så lenge verdier fins
            Rute c = (Rute)it.next();                                    //Henter verdi
            if(v == c.getValue()){                                       //Ser om v er lik
                ok = c.getIndex();
            }
        }
        return ok;                                                      //Returnerer svar
    }

    /**
     * Går gjennom alle elementene i brettet og ser om de er fylt inn
     * @param iter Iterator for brettet
     * @return true/false om brettet er fylt ut
     */
    public Boolean won(Iterator iter) {
        boolean ok = true;
        while(iter.hasNext()) {
            Rute c = (Rute)iter.next();
            if(c.getValue() == -1) {
                ok = false;
            }
        }
        return ok;
    }
}
