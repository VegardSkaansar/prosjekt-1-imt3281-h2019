package no.ntnu.imt3281.sudoku.logic.operation;

import no.ntnu.imt3281.sudoku.logic.cell.Rute;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Klassens oppgave er å laste brettet fra json fil
 * og eventuelt speile brettet enten horizontalt, vertikalt eller diagonalt
 * Inneholder metoder for å laste og transofrmere brettets verdier
 */
public class BoardTransformer {

    private static final Integer AMOUNT = 4;                                                     //Mulige load utfall

    private static final Logger LOGGER = Logger.getLogger(BoardTransformer.class.getName());     //Logger

    private BoardTransformer() {

    }

    /**
     * Laster brettet fra json fil, og returrnerer en tilfeldig speiling av brettet
     * @return En mulig speiling av sudoku brettet
     */
    public static List<Rute> startNewGame() {
        Random random = new Random(System.currentTimeMillis());
        switch(random.nextInt(AMOUNT + 1)){
            case 1:
                return mirrorHorizontal(load());                        //Vanrett speilet
            case 2:
                return mirrorDiagonal(load(), "right");            //Diagonalt speilet mot høyre
            case 3:
                return mirrorDiagonal(load(), "");                 //Diagonal speilet mot venstre
            case 4:
                return mirrorVertical(load());                         //Lodrett speilet
            default:
                return load();                                         //Null speiling
        }
    }

    /**
     * Load funksjon for testing sin skyld.
     * @return lastet liste fra json fil
     */
    public static List<Rute> testLoad(){
        return load();
    }

    /**
     * Load funksjon for å teste speiling
     * @param dir Vilken retning den skal speile
     * @return returnere en liste med diagonalt sudoku brett
     */
    public static List<Rute> testDiagonalLoad(String dir){
        return mirrorDiagonal(load(), dir);
    }

    /**
     * Laster dataene fra en json fil og fyller opp rutelisten
     * @return ArrayList med rute objekter, parset fra verdiene i json filen
     */
    private static ArrayList<Rute> load(){
        ArrayList<Rute> result = new ArrayList<>();
        int count = 0;
        //Parser JSON
        JSONParser jsp = new JSONParser();
        try(Reader re = new FileReader("./resources/no/ntnu/imt3281/sudoku/board.json")){
            JSONArray jsa = (JSONArray) jsp.parse(re);
            for(Object o : jsa){
                for(Object ob : (JSONArray)o){
                    //Lager ruter og setter dem til locked hvis de har et tall allrede
                    Rute r = new Rute(((Long)ob).intValue(), (((Long)ob).intValue() != -1), count);
                    result.add(r);
                    count ++;
                }
            }
        }
        catch (IOException|ParseException e){
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        return result;
    }

    /**
     * Speiler bretter vanrett ved å reversere rekkefølgen av hver rad
     * @param board brettet vi speiler
     * @return Speilet brett
     */

    private static ArrayList<Rute> mirrorHorizontal(ArrayList<Rute> board){
        ArrayList<Rute> mirrored = new ArrayList<>();                               //Speilet brett
        int ind = 0;                                                                //Index for speilede ruter
        Rute original;                                                              //Ruten vi speiler
        Rute newer;                                                                 //Ny rute
        for(int i = 8; i < 81; i+=9){                                               //Reverserer alle radene
            for(int j = i; j > i-9; j--){
                original = board.get(j);
                newer = new Rute(original.getValue(), original.isLocked(), ind);
                mirrored.add(newer);
                ind++;
            }
        }
        return mirrored;
    }

    /**
     * Speiler brettet lodrett ved å å reversere kolonnene
     * @param board brettet vi speiler
     * @return Speilet brett
     */
    private static ArrayList<Rute> mirrorVertical(ArrayList<Rute> board){
        ArrayList<Rute> mirrored = new ArrayList<>();                               //Speilet brett
        int ind = 0;                                                                //Index for speilede ruter
        Rute original;                                                              //Ruten vi speiler
        Rute newer;                                                                 //Ny rute
        for(int i = 72; i >= 0; i-=9){                                              //Reverserer alle kolonnene
            for(int j = i; j < (i+9); j++){
                original = board.get(j);
                newer = new Rute(original.getValue(), original.isLocked(), ind);
                mirrored.add(newer);
                ind++;
            }
        }

        return mirrored;
    }

    /**
     * Speiler brettet diagonalt enten fra ventre eller høyre.
     * Mot høyre vil si at fra top venstre, flyttes alt mot bunn høyre
     * Mot venstre betyr fra top høyre, flyttes alt mot bunn venstre
     * @param board brettet vi speiler
     * @param dir hvilken retning vi speiler
     * @return Speilet brett
     */
    private static ArrayList<Rute> mirrorDiagonal(ArrayList<Rute> board, String dir){
        ArrayList<Rute> mirrored = new ArrayList<>();                                       //Speilet brett
        int ind = 0;                                                                        //Index for speilede ruter
        Rute original;                                                                      //Ruten vi speiler
        Rute newer;                                                                         //Ny rute
        if(dir.equalsIgnoreCase("right")){                                     //Speiler mot høyre
            for(int i = 80; i >= 72; i--){                                                  //Jobber seg oppover hver kollonne, fra slutt
                for(int j = i; j >= 0; j-=9){
                    original = board.get(j);
                    newer = new Rute(original.getValue(), original.isLocked(), ind);        //Setter inn elemente av kollonene
                    mirrored.add(newer);
                    ind++;
                }
            }
        }
        else{                                                                               //Mot venstre
            for(int i = 0; i <= 8; i++){                                                    //Jobber seg nedover hver kolonne, fra start
                for(int j = i; j < 81; j+=9){
                    original = board.get(j);
                    newer = new Rute(original.getValue(), original.isLocked(), ind);        //Setter inn elementene av kollonene
                    mirrored.add(newer);
                    ind++;
                }
            }
        }
        return mirrored;
    }

    /**
     * Tar inn to verdier, og setter alle felt med hver verdi, lik den andre verdien.
     * @param numberOne Første verdi
     * @param numberTwo Andre verdi
     * @param list
     */
    public static void swapNumbers(int numberOne, int numberTwo, List<Rute> list){
        Iterator iterator = list.iterator();
        Rute rute;
        while (iterator.hasNext()){
            rute = (Rute)iterator.next();
            if(rute.getValue() == numberOne){
                rute.setValue(numberTwo);
            }
            else if(rute.getValue() == numberTwo){
                rute.setValue(numberOne);
            }
        }
    }
}

