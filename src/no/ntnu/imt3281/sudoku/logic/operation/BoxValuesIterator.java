package no.ntnu.imt3281.sudoku.logic.operation;

import no.ntnu.imt3281.sudoku.logic.cell.Rute;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Klassens oppgave er å returnere en iterator for alle verdiene i en boks, inkludert blanke
 * Brukes for å fylle grensensittet
 */
public class BoxValuesIterator implements Iterator {

    private ArrayList<Rute> values;     //Array med verdier
    private int pos;                    //Posisjonen i listen

    /**
     * Konstruktor fyller verdiene i iteratoren
     * @param start Index for første element i boksen
     * @param list Listen med verdiene i brettet
     */
    public BoxValuesIterator(int start, List<Rute> list){
        values = new ArrayList<>();
        pos = 0;
        for(int i = start; i < (start+20); i+=9){
            for(int j = i; j < (i+3); j++){
                values.add(list.get(j));
            }
        }
    }

    /**
     * Implementerer hasNext() fra Iterator.
     * @return true/false om det finnes flere elementer
     */
    @Override
    public boolean hasNext() {
        return (pos < values.size());
    }

    /**
     * Implementerer next() fra Iterator
     * @return Det neste objektet i values
     */
    @Override
    public Object next() {
        if(!hasNext()){ throw new NoSuchElementException(); }
        Rute c = values.get(pos);
        pos++;
        return c;
    }
}
