package no.ntnu.imt3281.sudoku.logic.cell;

/**
 * <h1>Rute</h1>
 * <p>Denne klassen har som jobb å passe på en rute.
 * Dette vil si at den passe på om den er låst og hva slags verdi bruker
 * har gitt den fra ett tekstfelt</p>
 */
public class Rute {

    // dette er verdien som starter på -1 og når den får en value
    // vil den være mellom 1-9.
    private Integer verdi;
    // denne variablen holder styr på om ruta eller cella er editable eller
    // ikke, false er åpen, og true er lukket.
    private Boolean lock;
    //Denne variablen forteller hvilken index ruten er
    private Integer index;

    /**
     * Konstruktor initsialiserer verdiene i ruten
     * @param v Verdien i ruten
     * @param l Om ruten er låst
     * @param i Indexen til ruten
     */
    public Rute(int v, boolean l, int i){
        verdi = v;
        lock = l;
        index = i;
    }

    /**
     * Returnerer verdien i ruten
     * @return verdien i ruten
     */
    public Integer getValue(){
        return verdi;
    }

    /**
     * Oppdaterer verdien ruten
     * @param v Verdien som skal settes in
     */
    public void setValue(int v){
        verdi = v;
    }

    /**
     * Returnerer om ruten er låst
     * @return true/false
     */
    public boolean isLocked() {
        return lock;
    }

    /**
     * Henter indexen til ruten
     * @return Rutens index
     */
    public Integer getIndex(){
        return index;
    }

    /**
     * Skriver ut ruten's verdi
     * @return Ruten's verdi, som en String
     */
    @Override
    public String toString(){
        return verdi.toString();
    }
}
