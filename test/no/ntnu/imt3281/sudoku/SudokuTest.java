package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;

import no.ntnu.imt3281.sudoku.logic.SudokuLogic;
import no.ntnu.imt3281.sudoku.logic.cell.Rute;
import no.ntnu.imt3281.sudoku.logic.operation.BoardSearcher;
import no.ntnu.imt3281.sudoku.logic.operation.BoardTransformer;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SudokuTest {

	@Test
    public void testLoadList(){
        SudokuLogic logic = new SudokuLogic();
        List<Rute> list = logic.getSudoku();
        assertEquals(81, list.size());
    }

    @Test
	public void testMirror(){
		List<Rute> diagonalLoad = BoardTransformer.testDiagonalLoad("right");
		List<Integer> expected = Arrays.asList(
				9, 5, -1, 6, 1, 3, -1, -1, -1,
				7, -1, 8, -1, -1, -1, 6, -1, -1,
				-1, -1, 2, -1, -1, -1, -1, -1, -1,
				-1, 9, -1, -1, 3, -1, -1, 5, -1,
				8, 1, -1, 2, -1, 6, -1, 9, 7,
				-1, 4, -1, -1, 8, -1, -1, 1, -1,
				-1, -1, -1, -1, -1, -1, 8, -1, -1,
				-1, -1, 6, -1, -1, -1, 9, -1, 3,
				-1, -1, -1, 7, 4, 8, -1, 6, 5
		);
		Iterator diagIter = diagonalLoad.iterator();
		Iterator expecIter = expected.iterator();
		int diagV;
		int expecV;
		assertTrue(diagonalLoad.size() == expected.size());
		while (diagIter.hasNext()){
			diagV = ((Rute)diagIter.next()).getValue();
			expecV = ((Integer)expecIter.next());
			assertTrue(diagV == expecV);
		}
	}

    @Test
	public void testGetRkb(){
		List<Integer> li = BoardSearcher.getRkb(30);
		int[] arr = new int[li.size()];
		for(int i = 0; i < li.size(); i++){
			arr[i] = li.get(i);
		}
		assertArrayEquals(new int[]{3, 3, 30}, arr);
	}

	@Test
	public void testGetBIndex(){
		int bIndex = BoardSearcher.getBIndex(5);
		assertTrue(bIndex == 33);
		bIndex = BoardSearcher.getBIndex(8);
		assertTrue(bIndex == 60);
	}

	@Test
	public void testInput(){
		SudokuLogic logic = new SudokuLogic(false);
		List<Rute> li = logic.getSudoku();
		int ind = 46;
		int failValue = 7;
		int passValue = 5;
		int oldValue = li.get(ind).getValue();
		assertFalse(logic.input(ind, failValue).isEmpty());
		assertTrue((oldValue == li.get(ind).getValue()));
		assertTrue(logic.input(ind, passValue).isEmpty());
		assertTrue((passValue == li.get(ind).getValue()));
	}

	@Test
	public void testBoardItearor(){
		SudokuLogic logic = new SudokuLogic(false);
		int rad = 9;
		int kolon = 5;
		int box = 27;
		int count = 0;
		Iterator boardIterRow = logic.getBoardIterator(rad, "row");
		Iterator boardIterColumn = logic.getBoardIterator(kolon, "column");
		Iterator boardIterBox = logic.getBoardIterator(box, "box");
		List<Integer> expectedRowVals = Arrays.asList(6,1,9,5);
		List<Integer> expectedColumnVals = Arrays.asList(5,3,9);
		List<Integer> expectedBoxVals = Arrays.asList(8,4,7);

		while(boardIterRow.hasNext()){
			int realVal = ((Rute)boardIterRow.next()).getValue();
			int expVal = expectedRowVals.get(count);
			assertEquals(realVal, expVal);
			count++;
		}
		count = 0;
		while(boardIterColumn.hasNext()){
			int realVal = ((Rute)boardIterColumn.next()).getValue();
			int expVal = expectedColumnVals.get(count);
			assertEquals(realVal, expVal);
			count++;
		}
		count = 0;
		while(boardIterBox.hasNext()){
			int realVal = ((Rute)boardIterBox.next()).getValue();
			int expVal = expectedBoxVals.get(count);
			assertEquals(realVal, expVal);
			count++;
		}
	}

	@Test
	public void testBoxValuesIterator(){
		SudokuLogic logic = new SudokuLogic(false);
		Iterator boxValIter = logic.getBoxValuesIterator(BoardSearcher.getBIndex(5));
		int value;
		int count = 0;
		List<Integer> expected = Arrays.asList(-1, -1, 3, -1, -1, 1, -1, -1, 6);
		while(boxValIter.hasNext()){
			value = ((Rute)boxValIter.next()).getValue();
			assertTrue(value == expected.get(count));
			count++;
		}
	}

	@Test
	public void testSwapNumbers(){
		SudokuLogic logic = new SudokuLogic(false);
		List<Rute> list = logic.getSudoku();
		BoardTransformer.swapNumbers(5, 3, list);
		int listVal = 0;
		int expecVal = 0;
		List<Integer> expected = Arrays.asList(
				3, 5, -1, -1, 7, -1, -1, -1, -1,
				6, -1, -1, 1, 9, 3, -1, -1, -1,
				-1, 9, 8, -1, -1, -1, -1, 6, -1,
				8, -1, -1, -1, 6, -1, -1, -1, 5,
				4, -1, -1, 8, -1, 5, -1, -1, 1,
				7, -1, -1, -1, 2, -1, -1, -1, 6,
				-1, 6, -1, -1, -1, -1, 2, 8, -1,
				-1, -1, -1, 4, 1, 9, -1, -1, 3,
				-1, -1, -1, -1, 8, -1, -1, 7, 9
		);
		assertTrue(list.size() == expected.size());
		Iterator listIter = list.iterator();
		Iterator expectedIter = expected.iterator();
		while (listIter.hasNext()){
			listVal = ((Rute)listIter.next()).getValue();
			expecVal = ((Integer)expectedIter.next());
			assertTrue(listVal == expecVal);
		}
	}

}
